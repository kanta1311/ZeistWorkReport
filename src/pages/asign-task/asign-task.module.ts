import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AsignTaskPage } from './asign-task';

@NgModule({
  declarations: [
    AsignTaskPage,
  ],
  imports: [
    IonicPageModule.forChild(AsignTaskPage),
  ],
})
export class AsignTaskPageModule {}
