import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-asign-task',
  templateUrl: 'asign-task.html',
})
export class AsignTaskPage {

  emp:any;
  Name:any;
  emp_list:any;
  today_date:any;
  minDate:any;
  maxDate:any;
  dateFrom:any;
  constructor(public alerCtrl: AlertController,public storage:Storage,public navCtrl: NavController, public navParams: NavParams,public http:Http,public toastCtrl:ToastController) 
  {
    this.storage.get("emp").then(val=>{
        this.emp=val;
        this.Name=val.Full_Name;
        // console.log("Employee Details",val);
    });
    this.getEmployeeDetails();
    
    this.minDate=new Date().getFullYear();
    this.maxDate=new Date().getFullYear()+3;
    this.dateFrom=new Date().toLocaleDateString();
  }
  getEmployeeDetails()
  {
    let link="https://kantapp.xyz/zeist/work/api/all_employee.php";
  
    this.http.get(link).map(res=>res.json()).subscribe(data=>{
      this.emp_list=data.employee;

      console.log(this.emp_list.length,this.emp_list);
      console.log(this.emp_list[0])
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AsignTaskPage');
  }

  doRadio() {
    let alert = this.alerCtrl.create();
    alert.setTitle('Select Employee');

    
    for(var i=0;i<this.emp_list.length;i++)
    {
      alert.addInput({
        type: 'radio',
        label: this.emp_list[i].name,
        value: this.emp_list[i],
        checked: false
      });

      console.log(i);
    }
    

   

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        
      }
    });

    alert.present().then(() => {
     
    });
  }
}
