import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  FullName:any;
  Email:any;
  Mobile:any;
  Password:any;
  data:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public http:Http,public toastCtrl: ToastController) 
  {
    // this.FullName="Arvind Kant";
    // this.Email="kanta1311@gmail.com";
    // this.Mobile="9769077985";
    // this.Password="123456";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  registerEmployee()
  {
    let link ="https://kantapp.xyz/zeist/work/api/register.php";
    let Emailpattern=/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    let Mobilepattern=/^[7-9][0-9]{9}$/;

    let fullname=this.FullName;
    let email=this.Email;
    let mobile=this.Mobile;
    let password=this.Password;

    if(!fullname || fullname=='')
    {
      this.toast("Please Enter Your Name");
    }
    else if(!Emailpattern.test(email))
    {
      this.toast("Please Enter Valid Email");
    }
    else if(!Mobilepattern.test(mobile))
    {
      this.toast("Please Enter Valid Mobile Number");
    }
    else if(!password || password=='')
    {
      this.toast("Please Enter Password");
    }
    else{
      let data =JSON.stringify({fullname,email,mobile,password});
      this.http.post(link,data).map(res=>res.json()).subscribe(data=>{
        console.log(data);
        if(data.code==1)
          {
            this.toast("Registration Complete");
          }
        else if(data.code==3)
          {
            this.toast("Email Already Exist");
          }
          else
          {
            this.toast("Somthing wrong please Contact Admin.");
          }

          this.FullName="";
          this.Email="";
          this.Mobile="";
          this.Password="";
      });
    }
      


    
  }

  toast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

}
