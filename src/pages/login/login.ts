import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  Email:any;
  Password:any;
  
  constructor(public storage:Storage,public navCtrl: NavController, public navParams: NavParams,public http:Http,public toastCtrl:ToastController) 
  {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  Login()
  {
    let Emailpattern=/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    let username=this.Email;
    let password=this.Password;
    
    if(!username || username=='')
    {
      this.toast("Please Enter Valid Email");
    }
    else if(!password || password=='')
    {
      this.toast("please Enter Password");
    }
    else
    {
      let data = JSON.stringify({username,password});
      let link="https://kantapp.xyz/zeist/work/api/login.php";
      this.http.post(link,data).map(res=>res.json()).subscribe(data=>{
        console.log(data);
        if(data.status=='data found')
          {
            this.navCtrl.setRoot(HomePage);
            this.storage.set("emp",data.data);
          }
          else{
            this.toast("Please Check Username Or Password");
          }
        
      });
    }
      
  }

  RegisterPage()
  {
    this.navCtrl.push("RegisterPage");
  }

  forgotPassword(){
    this.navCtrl.push("ForgotPasswordPage");
  }

  toast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

}
