import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  RegisterPage()
  {
    this.navCtrl.push("RegisterPage");
  }
  loginPage()
  {
    this.navCtrl.push("LoginPage");
  }

  asignTask(){
    this.navCtrl.push("AsignTaskPage");
  }
}
